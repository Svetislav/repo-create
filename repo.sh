#!/bin/bash

# Create a bitbucket/github/gitlab repository from CLI and initialize local git repository in current directory
# First argument is 'bitbucket', 'github' or 'gitlab'
# Second argument, if given, will be a repository name. Otherwise, current working directory name will be used as repository name

if [ $# -gt 0 ]
  then

  repo_name=$2

  dir_name=`basename $(pwd)`

  if [ "$repo_name" = "" ]
    then
    echo "Repo name (hit enter to use '$dir_name')?"
    read repo_name
  fi

  if [ "$repo_name" = "" ]
    then
    repo_name=$dir_name
  fi

  if [ $1 = "bitbucket" ]
    then
    username=`git config bitbucket.user`
    if [ "$username" = "" ]
      then
      echo "Could not find username, run 'git config --global bitbucket.user <username>' to create one."
      invalid_credentials=1
    fi

    pass=`git config bitbucket.pass`
    if [ "$pass" = "" ]
      then
      echo "Could not find password, run 'git config --global bitbucket.pass <pass>' to create one."
      invalid_credentials=1
    fi
  elif [ $1 = "github" ]
    then
    username=`git config github.user`
    if [ "$username" = "" ]
      then
      echo "Could not find username, run 'git config --global github.user <username>' to create one."
      invalid_credentials=1
    fi

    token=`git config github.token`
    if [ "$token" = "" ]
      then
      echo "Could not find token, run 'git config --global github.token <token>' to create one."
      invalid_credentials=1
    fi
  elif [ $1 = "gitlab" ]
    then
    username=`git config gitlab.user`
    if [ "$username" = "" ]
      then
      echo "Could not find username, run 'git config --global gitlab.user <username>' to create one."
      invalid_credentials=1
    fi

    token=`git config gitlab.token`
    if [ "$token" = "" ]
      then
      echo "Could not find token, run 'git config --global gitlab.token <token>' to create one."
      invalid_credentials=1
    fi
  else
    echo "Bad argument. First argument should be 'bitbucket', 'github' or 'gitlab'"
    return 1
  fi

  if [ "$invalid_credentials" == "1" ]
    then
    return 1
  fi

  git init
  git config branch.master.remote origin 2> /dev/null
  git config branch.master.merge refs/heads/master 2> /dev/null

  url=`git config remote.origin.url`

  if [ $1 = "bitbucket" ]
    then
    if [ "$url" = git@bitbucket.org:$username/$repo_name.git -o "$url" = "" ]
      then
      git remote add origin git@bitbucket.org:$username/$repo_name.git 2> /dev/null
      echo -n "Creating Bitbucket repository '$repo_name' ..."
      echo
      curl -X POST -u "$username:$pass" -H "Content-Type: application/json" -d "{ \"scm\": \"git\", \"is_private\": \"True\" }" "https://api.bitbucket.org/2.0/repositories/{$username}/{${repo_name,,}}" > /dev/null
      echo " done."
    else
      echo "It seems that this local repository is already associated with different remote repository ($url). Aborting!"
    fi
  elif [ $1 = "github" ]
    then
    if [ "$url" = git@github.com:$username/$repo_name.git -o "$url" = "" ]
      then
      git remote add origin git@github.com:$username/$repo_name.git 2> /dev/null
      echo -n "Creating Github repository '$repo_name' ..."
      curl -u "$username:$token" https://api.github.com/user/repos -d '{"name":"'$repo_name'"}' > /dev/null
      echo " done."
    fi
  elif [ $1 = "gitlab" ]
    then
    if [ "$url" = git@gitlab.com:$username/$repo_name.git -o "$url" = "" ]
      then
      git remote add origin git@gitlab.com:$username/$repo_name.git 2> /dev/null
      echo -n "Creating Gitlab repository '$repo_name' ..."
      curl -H "Content-Type:application/json" https://gitlab.com/api/v4/projects?private_token=$token -d "{ \"name\": \"$repo_name\" }" > /dev/null
      echo " done."
    else
      echo "It seems that this local repository is already associated with different remote repository ($url). Aborting!"
    fi
  fi
else
  echo "##############################################################################################################"
  echo "Create a bitbucket/github/gitlab repository from CLI and initialize local git repository in current directory."
  echo "The script takes at least one argument."
  echo "First argument should be 'bitbucket', 'github' or 'gitlab'."
  echo "Second argument (optional) is the name of the repository to be created (defaults to cwd name)."
  echo "##############################################################################################################"
fi
