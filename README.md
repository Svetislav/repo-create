> repo - create bitbucket/github/gitlab repository from CLI and initialize local git repository in current directory

## Description

Script takes either one or two arguments:

- First argument (mandatory) is 'bitbucket', 'github' or 'gitlab' - it tells on which hosting service a repository should be created
- Second argument (optional) is a repository name

## Usage

Go to the project folder and call the script:

``` bash
Create a bitbucket repository (name of the repository will be the same as the name of the folder from which the script is called)
  $ ./repo.sh bitbucket

Create a bitbucket repository (name of the repository will be test)
  $ ./repo.sh bitbucket test

Create a github repository (name of the repository will be the same as the name of the folder from which the script is called)
  $ ./repo.sh github

Create a github repository (name of the repository will be test)
  $ ./repo.sh github test

Create a gitlab repository (name of the repository will be the same as the name of the folder from which the script is called)
  $ ./repo.sh gitlab

Create a gitlab repository (name of the repository will be test)
  $ ./repo.sh gitlab test
```
